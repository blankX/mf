#include "utils.h"
#include "database.h"

#include "subcommands.h"

void subcommand_delete(const Parser& parser) {
    if (parser.arguments.size() < 2) {
        fprintf(stderr, HELP_TEXT, parser.program_name);
        exit(1);
    }

    int rc = 0;
    Database db = Database::find(false);
    std::vector<std::filesystem::path> paths = get_paths_relative_to_database(parser, db, &rc, false);
    Sqlite3Statement stmt(db.db, "DELETE FROM memes WHERE path = ?");

    for (const std::filesystem::path& i : paths) {
        stmt.reset();
        stmt.bind_text(1, i.native(), SQLITE_STATIC);
        db.db.exec(stmt);

        if (db.db.changes() <= 0) {
            fprintf(stderr, "failed to delete %s: no such file\n", i.c_str());
            rc = 1;
        }
    }

    exit(rc);
}
