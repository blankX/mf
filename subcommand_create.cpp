#include "database.h"

#include "subcommands.h"

void subcommand_create(const Parser& parser) {
    if (parser.arguments.size() != 1) {
        fprintf(stderr, HELP_TEXT, parser.program_name);
        exit(1);
    }

    Database db = Database::create_in_cwd();

    Sqlite3Statement stmt(db.db, R"EOF(CREATE VIRTUAL TABLE IF NOT EXISTS memes USING fts4(
        path TEXT PRIMARY KEY NOT NULL,
        source TEXT NOT NULL,
        description TEXT NOT NULL,
        miscinfo TEXT NOT NULL,
        matchinfo=fts3
    ))EOF");
    db.db.exec(stmt);
}
