#pragma once

#include <optional>
#include <filesystem>

struct Database; // forward declaration from database.h
#include "blankie/cliparse.h"

void output_meme(const char* path, const char* source, const char* description, const char* miscinfo, int fd = 1, bool edit = false);
std::vector<std::filesystem::path> get_paths_relative_to_database(const blankie::cliparse::Parser& parser, const Database& db, int* rc, bool needs_exist = true);
std::filesystem::path get_path_relative_to_database(const char* path, const Database& db);
