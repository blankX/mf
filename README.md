# mf

A serverless meme finder with full-text search (thanks to SQLite, but you have
to describe all the images yourself)

```
Usage: mf [-C=<directory>] create
  or   mf [-C=<directory>] list
  or   mf [-C=<directory>] search <search query>
  or   mf [-C=<directory>] prune

  or   mf [-C=<directory>] {info|show} <file>...
  or   mf [-C=<directory>] delete <file>...
  or   mf [-C=<directory>] edit <file>
  or   mf [-C=<directory>] get <file>
  or   mf [-C=<directory>] set <file> <source> <description> <misc info>

-C=<directory> changes the current working directory to <directory> before a
subcommand is executed. This causes the database to be looked up in
<directory>, all file paths are relative to <directory> and database creation
to be inside of <directory>

create creates a database in the current directory (or <directory>)

prune removes all metadata entries that correspond to non-existent files, then
vaccums the database

delete only deletes the metadata for the files specified, not the actual files
themselves

get outputs the file's path, source, description, and miscellaneous information
delimited by a null-byte
```
