#include <unistd.h>

#include "utils.h"
#include "database.h"

#include "subcommands.h"

void subcommand_search(const Parser& parser) {
    if (parser.arguments.size() != 2) {
        fprintf(stderr, HELP_TEXT, parser.program_name);
        exit(1);
    }

    Database db = Database::find(true);
    Sqlite3Statement stmt(db.db, "SELECT path, source, description, miscinfo FROM memes WHERE memes MATCH ?");
    bool first_item = true;

    stmt.bind_text(1, parser.arguments[1], SQLITE_STATIC);
    try {
        db.db.exec(stmt, [&]() {
            if (!first_item) {
                write(1, "================================================================================\n", 81);
            }
            first_item = false;

            output_meme(stmt.column_text(0), stmt.column_text(1), stmt.column_text(2), stmt.column_text(3));
        });
    } catch (const Sqlite3Exception& e) {
        // it'll be printed to the error log, so do nothing
    }

    if (first_item) {
        exit(1);
    }
}
