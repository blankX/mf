#include "utils.h"
#include "database.h"

#include "subcommands.h"

void subcommand_set(const Parser& parser) {
    // Sanity check the arguments passed
    if (parser.arguments.size() != 5) {
        fprintf(stderr, HELP_TEXT, parser.program_name);
        exit(1);
    }

    // Open the database and find the meme's path, relative to the database
    Database db = Database::find(false);
    std::filesystem::path path = get_path_relative_to_database(parser.arguments[1], db);

    // Update an existing meme, if any
    Sqlite3Statement update_stmt(db.db, "UPDATE memes SET source = ?, description = ?, miscinfo = ? WHERE path = ?");
    update_stmt.bind_text(1, parser.arguments[2], SQLITE_STATIC);
    update_stmt.bind_text(2, parser.arguments[3], SQLITE_STATIC);
    update_stmt.bind_text(3, parser.arguments[4], SQLITE_STATIC);
    update_stmt.bind_text(4, path.native(), SQLITE_STATIC);
    db.db.exec(update_stmt);

    // If there is not one, then create one
    if (db.db.changes() <= 0) {
        Sqlite3Statement insert_stmt(db.db, "INSERT INTO memes (path, source, description, miscinfo) VALUES (?, ?, ?, ?)");
        insert_stmt.bind_text(1, path.native(), SQLITE_STATIC);
        insert_stmt.bind_text(2, parser.arguments[2], SQLITE_STATIC);
        insert_stmt.bind_text(3, parser.arguments[3], SQLITE_STATIC);
        insert_stmt.bind_text(4, parser.arguments[4], SQLITE_STATIC);
        db.db.exec(insert_stmt);
    }
}
