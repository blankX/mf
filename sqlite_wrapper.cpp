#include "sqlite_wrapper.h"

Sqlite3::Sqlite3(const char* filename, bool readonly, bool create) {
    int flags = readonly ? SQLITE_OPEN_READONLY : SQLITE_OPEN_READWRITE;
    if (create) {
        flags |= SQLITE_OPEN_CREATE;
    }

    int error_code = sqlite3_open_v2(filename, &this->_db, flags, nullptr);
    if (error_code != SQLITE_OK) {
        Sqlite3Exception e(error_code, this);
        sqlite3_close_v2(this->_db);
        throw e;
    }

    sqlite3_extended_result_codes(this->_db, 1);
}

Sqlite3Statement::Sqlite3Statement(Sqlite3& db, const char* sql) {
    int error_code = sqlite3_prepare_v2(db.get(), sql, -1, &this->_stmt, nullptr);
    if (error_code != SQLITE_OK) {
        throw Sqlite3Exception(error_code, &db);
    }
}
