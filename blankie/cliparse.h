#pragma once

#include <string>
#include <vector>
#include <cassert>
#include <cstdint>
#include <exception>

namespace blankie {
namespace cliparse {

enum ArgumentRequired {
    Yes,
    Maybe,
    No,
};

struct Flag {
    const char* long_flag; // must not be null
    char short_flag; // may be '\0'
    bool required;
    ArgumentRequired argument_required;

    bool used;
    const char* argument; // may be null

    Flag(const char* long_flag_, char short_flag_, bool required_ = false, ArgumentRequired argument_required_ = ArgumentRequired::No)
            : long_flag(long_flag_), short_flag(short_flag_), required(required_), argument_required(argument_required_) {
        assert(this->long_flag != nullptr);
    }
};

struct Parser {
    const char* program_name; // may be null
    std::vector<Flag> flags;
    size_t arguments_min;
    size_t arguments_max;
    std::vector<const char*> arguments;

    void parse(int argc, char** argv);

    Flag* flag(const char* long_flag);
    Flag* flag(char short_flag);

    Parser(const char* program_name_, std::vector<Flag> flags_, size_t arguments_min_ = 0, size_t arguments_max_ = SIZE_MAX)
            : program_name(program_name_), flags(std::move(flags_)), arguments_min(arguments_min_), arguments_max(arguments_max_) {
        assert(this->arguments_max >= this->arguments_min);
    }

private:
    void _handle_long_flag(int argc, char** argv, int& offset, bool& parse_flags);
    void _handle_short_flags(int argc, char** argv, int& offset);
};

class exception : public std::exception {};

class missing_flag : public exception {
public:
    missing_flag(const char* long_flag_) : long_flag(long_flag_) {
        using namespace std::string_literals;
        this->_msg = "flag --"s + this->long_flag + " required, but not used";
    }
    const char* what() const noexcept {
        return this->_msg.c_str();
    }

    const char* long_flag;

private:
    std::string _msg;
};

class missing_argument : public exception {
public:
    missing_argument(const char* long_flag_) : long_flag(long_flag_) {
        using namespace std::string_literals;
        this->_msg = "flag --"s + this->long_flag + " requires an argument, but one is not passed";
    }
    const char* what() const noexcept {
        return this->_msg.c_str();
    }

    const char* long_flag;

private:
    std::string _msg;
};

class invalid_argument_count : public exception {
public:
    invalid_argument_count(size_t arguments_min_, size_t arguments_max_) : arguments_min(arguments_min_), arguments_max(arguments_max_) {
        using namespace std::string_literals;
        this->_msg = "too little or too many arguments provided, there must be "s
            + std::to_string(this->arguments_min) + " to " + std::to_string(this->arguments_max) + " arguments";
    }
    const char* what() const noexcept {
        return this->_msg.c_str();
    }

    size_t arguments_min;
    size_t arguments_max;

private:
    std::string _msg;
};

class unknown_flag : public exception {
public:
    unknown_flag(std::string long_flag) {
        using namespace std::string_literals;
        this->_msg = "unknown flag --"s + std::move(long_flag);
    }
    unknown_flag(char short_flag) {
        using namespace std::string_literals;
        this->_msg = "unknown flag -"s + short_flag;
    }
    const char* what() const noexcept {
        return this->_msg.c_str();
    }

private:
    std::string _msg;
};

class unnecessary_argument : public exception {
public:
    unnecessary_argument(const char* long_flag_) : long_flag(long_flag_) {
        using namespace std::string_literals;
        this->_msg = "flag --"s + this->long_flag + " does not take in an argument, yet one is passed";
    }
    const char* what() const noexcept {
        return this->_msg.c_str();
    }

    const char* long_flag;

private:
    std::string _msg;
};

} // namespace cliparse
} // namespace blankie
