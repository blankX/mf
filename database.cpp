#include <cstdio>

#include "database.h"

Database Database::find(bool readonly) {
    std::filesystem::path directory = std::filesystem::current_path();
    do {
        std::filesystem::path path = directory / "mf.db";
        if (std::filesystem::is_regular_file(path)) {
            return Database(path, readonly, false);
        }
        directory = directory.parent_path();
    } while (directory != directory.root_path());

    fprintf(stderr, "Failed to find a database; have you created one and are in a directory with it?\n");
    exit(1);
}
