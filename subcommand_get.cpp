#include "utils.h"
#include "database.h"

#include "subcommands.h"

void subcommand_get(const Parser& parser) {
    // Sanity check the arguments passed
    if (parser.arguments.size() != 2) {
        fprintf(stderr, HELP_TEXT, parser.program_name);
        exit(1);
    }

    // Open the database and find the meme's path, relative to the database
    Database db = Database::find(true);
    std::filesystem::path path = get_path_relative_to_database(parser.arguments[1], db);

    // Select the meme, if any
    bool meme_found = false;
    Sqlite3Statement select_stmt(db.db, "SELECT source, description, miscinfo FROM memes WHERE path = ?");
    select_stmt.bind_text(1, path.native(), SQLITE_STATIC);
    db.db.exec(select_stmt, [&]() {
        meme_found = true;
    }, 1);

    // Output the meme's metadata to stdout
    printf("%s%c%s%c%s%c%s",
        path.c_str(), 0,
        meme_found ? select_stmt.column_text(0) : "", 0,
        meme_found ? select_stmt.column_text(1) : "", 0,
        meme_found ? select_stmt.column_text(2) : "");
}
