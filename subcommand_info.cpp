#include <unistd.h>

#include "utils.h"
#include "database.h"

#include "subcommands.h"

void subcommand_info(const Parser& parser) {
    if (parser.arguments.size() < 2) {
        fprintf(stderr, HELP_TEXT, parser.program_name);
        exit(1);
    }

    int rc = 0;
    bool first_item = true;
    Database db = Database::find(true);
    std::vector<std::filesystem::path> paths = get_paths_relative_to_database(parser, db, &rc);
    Sqlite3Statement stmt(db.db, "SELECT source, description, miscinfo FROM memes WHERE path = ?");

    for (const std::filesystem::path& i : paths) {
        bool item_found = false;

        stmt.reset();
        stmt.bind_text(1, i.native(), SQLITE_STATIC);
        db.db.exec(stmt, [&]() {
            if (!first_item) {
                write(1, "================================================================================\n", 81);
            }
            first_item = false;
            item_found = true;

            output_meme(i.c_str(), stmt.column_text(0), stmt.column_text(1), stmt.column_text(2));
        });

        if (!item_found) {
            fprintf(stderr, "failed to find %s\n", i.c_str());
            rc = 1;
        }
    }

    exit(first_item ? 1 : rc);
}
