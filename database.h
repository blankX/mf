#pragma once

#include <filesystem>

#include "sqlite_wrapper.h"

struct Database {
    Database(std::filesystem::path path_, bool readonly, bool create) : path(std::move(path_)), db(this->path.c_str(), readonly, create) {}

    static Database find(bool readonly);
    static Database create_in_cwd() {
        return Database(std::filesystem::current_path() / "mf.db", false, true);
    }

    std::filesystem::path path;
    Sqlite3 db;
};
