#pragma once

#include "blankie/cliparse.h"

using Parser = blankie::cliparse::Parser;

#define HELP_TEXT &R"EOF(
Usage: %1$s [-C=<directory>] create
  or   %1$s [-C=<directory>] list
  or   %1$s [-C=<directory>] search <search query>
  or   %1$s [-C=<directory>] prune

  or   %1$s [-C=<directory>] {info|show} <file>...
  or   %1$s [-C=<directory>] delete <file>...
  or   %1$s [-C=<directory>] edit <file>
  or   %1$s [-C=<directory>] get <file>
  or   %1$s [-C=<directory>] set <file> <source> <description> <misc info>

-C=<directory> changes the current working directory to <directory> before a
subcommand is executed. This causes the database to be looked up in
<directory>, all file paths are relative to <directory> and database creation
to be inside of <directory>

create creates a database in the current directory (or <directory>)

prune removes all metadata entries that correspond to non-existent files, then
vaccums the database

delete only deletes the metadata for the files specified, not the actual files
themselves

get outputs the file's path, source, description, and miscellaneous information
delimited by a null-byte
)EOF"[1]

void subcommand_create(const Parser& parser);
void subcommand_list(const Parser& parser);
void subcommand_search(const Parser& parser);
void subcommand_prune(const Parser& parser);

void subcommand_info(const Parser& parser);
void subcommand_delete(const Parser& parser);
void subcommand_edit(const Parser& parser);
void subcommand_get(const Parser& parser);
void subcommand_set(const Parser& parser);
